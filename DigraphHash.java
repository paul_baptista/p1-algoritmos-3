/**
* Clase concreta de DiGraph
* Uds debe seleccionar elmodelo de representacion que mejor
* satisfaga las restriciones establecidas
*
* Sustituya Hash por la palabra que mejor represente el modelo
* seleccionado
*/

public class DigraphHash extends Digraph{

  /*
  * Modelo de representacion: Hash con manejo de Colisiones.
  *
  */
   private MiTablaHash Nodos;
   private MiTablaHash VerticesDst;
   private MiTablaHash VerticesSrc;
 
  /**
  * Construye un grafo vacio.
  */
  public DigraphHash() {
    int t = 300;
    Nodos = new MiTablaHash(t);
    VerticesDst = new MiTablaHash(t);
    VerticesSrc = new MiTablaHash(t);
  }

   /**
   * Agrega la arista dada al grafo. Si los vertices de la arista
   * no existen o el grafo tiene una arista entre dichos vertices,
   * retorna false. Si se agrega la nueva arista, retorna true.
   * 
   * Complejidad Propuesta: O(p + q), p << | V | y q << | E |
   * Complenidad Real: O(1), ya que los nodos se agregan al final de las listas,
   * y el orden de add de Hash es O(1).
   */
  public boolean add(Edge e){
    Node nodo1 = new Node(e.getSrc());
    Node nodo2 = new Node(e.getDst());

    if((Nodos.contiene(nodo1,nodo1.toString()))
       &&(Nodos.contiene( nodo2,nodo2.toString())))
         return ((VerticesSrc.agregar(e,e.getSrc()))
                 &&(VerticesDst.agregar(e,e.getDst())));
    return false;
  }
   

   /**
   * Agrega el nodo n. Si el vertice ya existe, retorna false. Si
   * se agrega el nodo, retorna true.
   *
   * Complejidad: O(p), p << | V |
   * Complenidad Real: O(1), ya que los nodos se agregan al final de las listas,
   * y el orden de add de Hash es O(1).
   */
  public boolean add(Node n){
    if(!(Nodos.contiene(n,n.toString()))){
      if(!(Nodos.agregar(n,n.toString()))){
        return false;
      }else{
        return true;
      }
    }else{
        return false;
    }
  }

  /*
  * Elimina los nodos y aristas del grafo.
  * 
  * Complejidad: O(1)
  * Complejidad Real: O(1)
  */
  public void clear(){
    int t = 300;
    Nodos = new MiTablaHash(t);
    VerticesDst = new MiTablaHash(t);
    VerticesSrc = new MiTablaHash(t);
  }


  /*
  * Chequea que el grafo contiene una arista (src, dst).
  * 
  * Complejidad: O(p), p << | E |
  * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la cantidad de contenedores
  */
  public boolean contains(String src, String dst){
    Edge e = new Edge(src, dst);
    if((VerticesSrc.contiene(e,src))&&(VerticesDst.contiene(e,dst))){
      return true;
    }else{
      return false;
    }

  }

  /*
  * Chequea que el grafo contiene un nodo con el id del nod
  *
  * Complejidad: O(p), p << | V |
  * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la cantidad de contenedores
  */
  public boolean contains(String nod) {
    Node n = new Node(nod);
    if((Nodos.contiene(n,n.toString()))){
       return true;
    }else{
       return false;
    }
  }

   /*
   * Retorna la arista del grafo que conecta a los vertices
   * src y dst. Si no existe dicha arista, retorna null.
   * 
   *  Complejidad: O(p), p << | E |
   * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la cantidad de contenedores
   */
   public Edge get(String src, String dst){
      Edge e = new Edge(src,dst);
      if((VerticesSrc.contiene(e,src))&&(VerticesDst.contiene(e, dst))){        
         return e;
      }else{
         return null;
      }
   }

  /*
  * Retorna todas las aristas del grafo
  *
  * Complejidad: O(n)
  * Donde n es la cantidad de aristas
  */
  public List getEdges(){
    return VerticesSrc.obtenerNodos();
  }

   /*
   * Retorna el nodo con id nod. Si no existe dicho nodo, 
   * retorna null.
   *
   * Complejidad: O(p), p << |V|
   * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la 
   *   cantidad de contenedores
   */
   
   public Node get(String nod){
      Node n = new Node(nod);
      if(Nodos.contiene(n,n.toString())){
         return n;
      }else{
         return null;
      }
   }

   /* 
   * Retorna todos los nodos del grafo.
   *
   * Complejidad: O(n)
   * Donde n es la cantidad de nodos
   */
   public List getNodes(){
      return Nodos.obtenerNodos();
   }

  /*
   * Retorna los sucesores del nodo con id node
   *
   * Complejidad: O(p), p << | E |
   * Complejidad Real: O(k) donde k es el tamaño de la lista
   */
  public  List getSucs(String n){
    Node e = new Node(n);
    Node nodo_temporal;
    Edge arco_a_revisar;
    Object temporal;
    ListIterator iterador_src;
    List colisiones_src;
    List ListaLados = new MyList();
    if(Nodos.contiene(e,e.toString())){
      colisiones_src=VerticesSrc.ListaColisiones(n);
      if (!(colisiones_src.isEmpty())&&(colisiones_src != null)){
        iterador_src =  colisiones_src.iterator();
        while(iterador_src.hasNext()){
            temporal = iterador_src.next();
            if (!(temporal instanceof Edge))
              return ListaLados;

              arco_a_revisar = (Edge) temporal;

            if(arco_a_revisar.verificarSrc(n)){
              nodo_temporal = get(n);
              ListaLados.add(arco_a_revisar.getDst());
            }
        }
        return ListaLados;
      }else{
        return ListaLados;
      }
    }else{
      return ListaLados;
    }
  }

  /*
  * Retorna los sucesores del nodo con id node
  *
  * Complejidad: O(p), p << | E |
  * Complejidad Real: O(k) donde k es el tamaño de la lista
  */
  public  List getPreds(String n){
    Node e = new Node(n);
    Node nodo_temporal;
    Edge arco_a_revisar;
    Object temporal;
    ListIterator iterador_dst;
    List colisiones_dst;
    List ListaLados = new MyList();
    if(Nodos.contiene(e,e.toString())){
      colisiones_dst=VerticesDst.ListaColisiones(n);
      if (!(colisiones_dst.isEmpty())&&(colisiones_dst != null)){
        iterador_dst =  colisiones_dst.iterator();
        while(iterador_dst.hasNext()){
            temporal = iterador_dst.next();
            if (!(temporal instanceof Edge))
              return ListaLados;

              arco_a_revisar = (Edge) temporal;

            if(arco_a_revisar.verificarDst(n)){
              nodo_temporal = get(n);
              ListaLados.add(arco_a_revisar.getSrc());
            }
        }
        return ListaLados;
      }else{
        return ListaLados;
      }
    }else{
      return ListaLados;
    }
  }



   /*
   * Retorna el numero de aristas en el grafo.
   *
   * Complejidad: O(1)
   */
   public int getNumEdges() {
      return VerticesDst.obtenerCantidadListaOcupado();
   }

   /*
   * Retorna el numero de vertices en el grafo.
   * 
   * Complejidad: O(1)
   */
   public int getNumVertices(){
      return Nodos.obtenerCantidadListaOcupado();
   }

  /*
   * Retorna la lista de lados que tienen al vertice dado como
   * origen. Si el vertice no existe, retorna null.
   * 
   * Complejidad: O(p), p << | E |
   * Complejidad Real: O(k) donde k es el tamaño de la lista
   */
  public List getOutEdges(String n){
    Node e = new Node(n);
    Node nodo_temporal;
    Edge arco_a_revisar;
    Object temporal;
    ListIterator iterador_src;
    List colisiones_src;
    List ListaLados = new MyList();
    if(Nodos.contiene(e,e.toString())){
      colisiones_src=VerticesSrc.ListaColisiones(n);
      if (!(colisiones_src.isEmpty())&&(colisiones_src != null)){
        iterador_src =  colisiones_src.iterator();
        while(iterador_src.hasNext()){
            temporal = iterador_src.next();
            if (!(temporal instanceof Edge))
              return ListaLados;

              arco_a_revisar = (Edge) temporal;

            if(arco_a_revisar.verificarSrc(n)){
              nodo_temporal = get(n);
              ListaLados.add(arco_a_revisar);
            }
        }
        return ListaLados;
      }else{
        return ListaLados;
      }
    }else{
      return ListaLados;
    }
  }

  /*
   * Retorna la lista de lados que tienen al vertice dado como
   * destino. Si el vertice no existe, retorna null.
   * 
   * Complejidad: O(p), p << | E |
   * Complejidad Real: O(k) donde k es el tamaño de la lista
   */

  public List getInEdges(String n){
  Node e = new Node(n);
    Node nodo_temporal;
    Edge arco_a_revisar;
    Object temporal;
    ListIterator iterador_dst;
    List colisiones_dst;
    List ListaLados = new MyList();
    if(Nodos.contiene(e,e.toString())){
      colisiones_dst=VerticesDst.ListaColisiones(n);
      if (!(colisiones_dst.isEmpty())&&(colisiones_dst != null)){
        iterador_dst =  colisiones_dst.iterator();
        while(iterador_dst.hasNext()){
            temporal = iterador_dst.next();
            if (!(temporal instanceof Edge))
              return ListaLados;

              arco_a_revisar = (Edge) temporal;

            if(arco_a_revisar.verificarDst(n)){
              nodo_temporal = get(n);
              ListaLados.add(arco_a_revisar);
            }
        }
        return ListaLados;
      }else{
        return ListaLados;
      }
    }else{
      return ListaLados;
    }
  }


   /*
   * Remueve la arista del grafo que conecta a los vertices src y
   * dst. Si el grafo no cambia, retorna false. Si el grafo cambia,
   * retorna true.
   * 
   * Complejidad: O(p), p << | E |
   * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la 
   *   cantidad de contenedores
   */
   public boolean remove(String src, String dst){
      Edge e = new Edge(src, dst);
      return VerticesSrc.eliminar(e, src) && VerticesDst.eliminar(e, dst);
   }

   /*
   * Remueve el nodo del grafo con id nod. Si el grafo no cambia,
   * retorna false. Si el grafo cambia, retorna true.
   *
   * Complejidad: O(p), p << | V |
   * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la 
   *   cantidad de contenedores
   */

  public boolean remove(String n){
    Node e = new Node(n);
    List colisiones_src;
    List colisiones_dst;
    Object temporal;
    Edge arco_a_revisar;
    ListIterator iterador_src; 
    ListIterator iterador_dst;
    Boolean retorno = false;

    if(Nodos.contiene(e,n)){
      if(Nodos.eliminar(e,n)){
        colisiones_src = VerticesSrc.ListaColisiones(n);
        if (colisiones_src != null){
          iterador_src =  colisiones_src.iterator();
          while(iterador_src.hasNext()){
              temporal = iterador_src.next();
              if (!(temporal instanceof Edge))
                return false;

              arco_a_revisar = (Edge) temporal;

              if(arco_a_revisar.verificarSrc(n)){
                iterador_src.unlink();
              }
          }
          retorno = true;
        }else{
          retorno = false;
        }
        colisiones_dst = VerticesDst.ListaColisiones(n);
        if (colisiones_dst != null){
          iterador_dst =  colisiones_dst.iterator();
          while(iterador_dst.hasNext()){
            temporal = iterador_dst.next();
            if (!(temporal instanceof Edge))
              return false;

            arco_a_revisar = (Edge) temporal;

            if(arco_a_revisar.verificarDst(n)){
              iterador_dst.unlink();
            }
          }
          retorno = true;
        }else{
          retorno = false;
        }
      }else{
        retorno = false;
      }
    }
    return retorno;
  }

  /*
    Complejidad Real: O(k) donde k es la cantidad de contenedores
  */
  public DigraphHash clone(){
    DigraphHash Hash  = new DigraphHash();
    Hash.Nodos = Nodos.clone();
    Hash.VerticesDst = VerticesDst.clone();
    Hash.VerticesSrc= VerticesSrc.clone();
    return Hash;
  }

  /* 
    Complejidad Real: O(n) donde n es la cantidad de elementos
  */
  public void Imprimir() {
    List Lista_Nodos = Nodos.obtenerNodos();
    List Lista_Vertices = VerticesSrc.obtenerNodos();
    ListIterator iterator_nodos = Lista_Nodos.iterator();
    ListIterator iterator_vertices = Lista_Vertices.iterator();
    Object temporal;
    Edge arco_a_revisar;
    Node nodo_a_revisar;

    while (iterator_nodos.hasNext()) {
        temporal = iterator_nodos.next();
        if(temporal instanceof Node){
            nodo_a_revisar = (Node) temporal;
            System.out.println(nodo_a_revisar.toString()); 
        }
    }

    while (iterator_vertices.hasNext()) {
        temporal = iterator_vertices.next();
        if(temporal instanceof Edge){
            arco_a_revisar = (Edge) temporal;
            System.out.println(arco_a_revisar.toString()); 
        }
    }
  }

}

// End Digraph.java