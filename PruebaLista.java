/*
 * Archivo: Main.java 
 * Descripcion: Archivo de prueba. Prueba todas las
 * funcionalidades implementadas en forma ordenada y clara.
 * Autor: X (carnet) y Y (carnet). Grupo Z
 * Fecha:
 */

public class PruebaLista {
   
   
   public static void println(Object... args) {
      for(Object pts: args)
         System.out.print(pts);
      System.out.println();
   }
   
   public static void printlist(List<String> lista) {
      ListIterator<String> iter = lista.iterator();
      while (iter.hasNext()) {
         println(iter.next());      
      }
   }

   public static void agregarElems(List<String> lista, int n){
      for (int i = 1; i <= 10; i++) {   
      lista.add("Prueba" + i);
         if (lista.isEmpty()) {
            println("La lista esta vacia");
         } 
      }
   }
   
   
   public static void main(String args[]) {
      
      println("Creando las listas\n");
      
      List<String> lista = new MyList<String>();
      List<String> lista2 = new MyList<String>();
      List<String> lista3 = new MyList<String>();
      
      ListIterator<String> iter = lista.iterator();
      ListIterator<String> iter2 = lista2.iterator();
      ListIterator<String> iter3 = lista3.iterator();
      
      println("\"lista\" esta vacía: "+lista.isEmpty());
      println("\n\"lista2\" esta vacía: "+lista2.isEmpty());
      println("\n\"lista3\" esta vacía: "+lista3.isEmpty());

      //Agregamos 10 elementos a lista
      println("\nAgregando 10 elementos a \"lista\"...");
      agregarElems(lista, 10);
      println("\n\"lista\" ahora tiene " + lista.getSize() + " elementos\n");
      
      if (iter.hasNext()) {
         println("El iterador de \"lista\" si tiene un siguiente y funciona");
      } else {
         println("Se cago el iterador de \"lista\"");
      }
      
      for (int i=1;i<=10;i++) {
         println("El elemento \"Prueba"+i
                 +"\"pertenece a lista: "+lista.contains("Prueba"+i));
      }

      if (iter2.hasNext()) {
         println("El iterador de \"lista2\" si tiene un siguiente y funciona");
      } else {
         println("Se cagó el iterador de \"lista2\"");
      }
      
      println("\nAgregamos dos cualquiervainas a \"lista2\"");
      lista2.add("cualquiervaina");
      lista2.add("cualquierotravaina");
      println("tamano de lista2 = " + lista2.getSize());
      
      if (iter2.hasNext()) {
         println("El iterador de lista2 si tiene un siguiente y funciona");
      } else {
         println("Se cago el iterador");
      }

      println("\nBorramos Prueba5 de lista");
      println(lista.remove("Prueba5"));

      println("\nRecuperamos los elementos la lista y los imprimimos");
      printlist(lista);
      println(lista.getSize());
            
      println("\nRecuperamos los elementos de la segunda lista y los imprimimos");
      printlist(lista2);
      
      // println("\nBorraremos a (cualquiervaina)\n");

      // println(lista2.remove(1));

      printlist(lista2);
      println("\n");
      
      println("\nProbando con la primera lista, haciendo unlink de 10 elementos");
      
      println("\nHacemos unlink de \"lista\"");

      iter = lista.iterator();
      while (iter.hasNext()){
         iter.next();   
      }
      
      while (lista.getSize()>0) {
         iter.unlink();
      } 	
      println("\nListo el unlink");
      printlist(lista);
      println(lista.getSize());
      println();

      for (int i=1;i<=10;i++) {
         println("El elemento \"Prueba"+i
                 +"\"pertenece a lista: "+lista.contains("Prueba"+i));
      }
      println("El elemento \"Prueba93\"pertenece a lista: "
              +lista.contains("Prueba93"));

      println();
      println("El elemento \"cualquiervaina\"pertenece a lista2: "
              +lista2.contains("cualquiervaina"));

      println("El elemento \"cualquierotravaina\"pertenece a lista: "
              +lista2.contains("cualquierotravaina"));
      
      println();
      
      println("lista es igual a lista2: "+lista.equals(lista2));

      println("\nAgregando 10 elementos a \"lista\"...");
      agregarElems(lista, 10);
      printlist(lista);

      println("\nAgregando 10 elementos a \"lista3\"...");
      agregarElems(lista3, 10);
      printlist(lista3);
        
      println("\nlista es igual a lista2: "+lista.equals(lista));

      println("\n\nConcat lista2 y lista3: \n");

      println(lista2.Concat(lista3));

      printlist(lista2);

      println();

      printlist(lista3);


   }
}
