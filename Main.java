/*
 * Archivo: Main.java 
 * Descripcion: Archivo de prueba. Prueba todas las
 * funcionalidades implementadas en forma ordenada y clara.
 * Autor: Paul Baptista (10-10056) y Fabio Castro (10-10132). Grupo Z
 * Fecha:
 */
import java.io.*; 

public class Main {


   /*
    * Funcion println, imprime en pantalla la representación en String de los
    *   objetos que recibe como parametros
    */
   public static void println(Object... args) {
      for(Object pts: args)
         System.out.print(pts);
      System.out.println();
   }


   /*
    * Funcion ClearScreen imprime 25 saltos de linea y limpiando el terminal
    */

   public static void ClearScreen() {
      int i;
      for(i=0;i<=25;i++){
         println();
      }

   }
   /*
    * Funcion printlist, imprime en pantalla la representación en String de los
    *   objetos que contiene la lista pasada como parametro
    */
   public static void printlist(List lista) {
      ListIterator iter = lista.iterator();
      while (iter.hasNext()) {
         println(iter.next());      

      }
   }



   /*
      Main Principal que ejecuta todas las pruebas necesarias
   */
   public static void main(String args[]){



      /*
      * Se manejan las excepciones  de la lectura de teclado
      */

      try{
         /*
         * Se lee la entrada de teclado por medio de un buffer, y se asigna a
         * una variable
         */        
         DigraphHash grafo = new DigraphHash();
         String texto;
         String texto2;
         boolean salida = true;
         InputStreamReader leer = new InputStreamReader(System.in);
         BufferedReader buff = new BufferedReader(leer);

         println("Bienvenido a la prueba de la  implementación de DiGraph");
         println("Para comenzar a probar se debe realizar una carga inicial.");
         println("Indique cuantos nodos desea agregar: ");
         texto = buff.readLine();
         int cantidad = Integer.parseInt(texto);
         int i;
         int numero;

         for(i=1;i<=cantidad;i++){
           println("Escriba un elemento: ");
           buff = new BufferedReader(leer);
           texto = buff.readLine();
           println(grafo.add(new Node(texto)));
           println();
         }

         System.out.print("Indique cuantos arcos desea agregar: ");
         texto = buff.readLine();
         cantidad = Integer.parseInt(texto);

         for(i=1;i<=cantidad;i++){
           println("Escriba el nodo origen: ");
           buff = new BufferedReader(leer);
           texto = buff.readLine();
           println("Escriba el nodo destino: ");
           buff = new BufferedReader(leer);
           texto2 = buff.readLine();
           println(grafo.add(new Edge(texto,texto2)));
           println();
         }

         while(salida){
         ClearScreen();
         println("1) Agrega un elemento al grafo");
         println("2) Agrega un vertice al grafo");
         println("3) Imprimir numero de Arcos");
         println("4) Imprimir numero de Vertices");
         println("5) Devuelve una lista de los arcos entrantes dado un nodo");
         println("6) Devuelve una lista de los arcos salientes dado un nodo");
         println("7) Devuelve una lista con todos los nodos del grafo");
         println("8) Devuelve un nodo dado su ID");
         println("9) Devuelve una lista con todos los arcos del grafo");
         println("10) Devuelve un arco, dado su origen y destino");
         println("11) Devuelve una lista con los Predecesores al nodo dado");
         println("12) Devuelve una lista con los Sucesores al nodo dado");
         println("13) Booleano que indica si el arco existe en el grafo");
         println("14) Devuelve un booleano que indica si el nodo existe en el grafo");
         println("15) Elimina el arco del grafo, dado su Src y Dst");
         println("16) Elimina el nodo del grafo, dado su id");
         println("17) Imprime el grafo");
         println("18) Elimina el Grafo");
         println("19) Clona el Grafo");
         println("20) Salir");
         println("Indique su opcion:  ");
         buff = new BufferedReader(leer);
         texto = buff.readLine();
         numero = Integer.parseInt(texto);
         println();


         switch(numero) {
          case 1: 
            println("Indique el nodo que desea agregar");
            buff = new BufferedReader(leer);
            texto = buff.readLine();
            println(grafo.add(new Node(texto)));
            println();
            break;
          case 2: 
            println("Escriba el nodo origen ");
            buff = new BufferedReader(leer);
            texto = buff.readLine();
            println("Escriba el nodo destino ");
            buff = new BufferedReader(leer);
            texto2 = buff.readLine();
            println(grafo.add(new Edge(texto,texto2)));
            println();
            break;
          case 3: 
            println(grafo.getNumEdges());
            println();
            break;
          case 4: 
            println(grafo.getNumVertices());
            println();
            break;
          case 5:
            println("Indique el nodo");
            buff = new BufferedReader(leer);
            texto = buff.readLine();
            printlist(grafo.getInEdges(texto));
            println();
            break;
          case 6:
            println("Indique el nodo");
            buff = new BufferedReader(leer);
            texto = buff.readLine();
            printlist(grafo.getOutEdges(texto));
            println();
            break;
          case 7:
            printlist(grafo.getNodes());
            println();          
            break;
          case 8: 
            println("Indique el nodo");
            buff = new BufferedReader(leer);
            texto = buff.readLine();
            println(grafo.get(texto));
            println();
            break;
          case 9: 
            printlist(grafo.getEdges());
            println();
            break;
          case 10: 
            println(" Escriba el nodo origen ");
            buff = new BufferedReader(leer);
            texto = buff.readLine();
            println(" Escriba el nodo destino ");
            buff = new BufferedReader(leer);
            texto2 = buff.readLine();
            println(grafo.get(texto,texto2));
            println();
            break;
          case 11:
            println("Indique el nodo");
            buff = new BufferedReader(leer);
            texto = buff.readLine();          
            printlist(grafo.getPreds(texto));
            println();            
            break;
          case 12:
            println("Indique el nodo");
            buff = new BufferedReader(leer);
            texto = buff.readLine();  
            printlist(grafo.getSucs(texto));
            println();
            break;
          case 13:
            println(" Escriba el nodo origen ");
            buff = new BufferedReader(leer);
            texto = buff.readLine();
            println(" Escriba el nodo destino ");
            buff = new BufferedReader(leer);
            texto2 = buff.readLine();
            println(grafo.contains(texto,texto2));
            println();           
            break;
          case 14:
            println("Indique el nodo");
            buff = new BufferedReader(leer);
            texto = buff.readLine();  
            println(grafo.contains(texto));
            println();
            break;
          case 15:
            println(" Escriba el nodo origen ");
            buff = new BufferedReader(leer);
            texto = buff.readLine();
            println(" Escriba el nodo destino ");
            buff = new BufferedReader(leer);
            texto2 = buff.readLine();
            println(grafo.remove(texto,texto2));
            println();
            break;
          case 16:
            println("Indique el nodo");
            buff = new BufferedReader(leer);
            texto = buff.readLine();  
            println(grafo.remove(texto));
            println();
           break;
          case 17: 
           grafo.Imprimir();
            break;
          case 18: 
           grafo.clear();
            break;
          case 19: 
            println("Se clona y se imprime el grafo clonado");
            DigraphHash grafo2 = grafo.clone();
            grafo2.Imprimir();
            break; 
          case 20:
            salida = false;
            break;   
          default: 
               println("Error");
          break;
         }
         println("Presione Enter Para Continuar");
         buff = new BufferedReader(leer);
         texto = buff.readLine();
      }




      }

      catch(java.io.IOException ioex) {}



   }

}