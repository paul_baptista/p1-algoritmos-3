/* Interfaz que define el comportamiento de un Hash.
* 
* Esta es una clase parametrizada con tipo (clase) E; i.e., la
* lista contiene elementos de tipo E.
*/


  public interface TablaHash<E>{

   /**
    *Agrega un Elemento a la tabla de Hash, usando el identificador como clave.
    */
    public boolean agregar(E objeto, String identificador);

   /**
    *Elimina un par (clave,valor) de la tabla de Hash
    */
    public boolean eliminar(E objeto, String identificador);

   /**
    * Reinicializa la tabla de Hash
    */
    public void borrado_completo();

   /**
    * Verifica que la tabla contenga un par (clave,valor)
    */
    public boolean contiene(E objeto, String identificador);

   /**
    * Retorna una Lista con todos los elementos de la tabla
    */
    public List obtenerNodos();

    // public TablaHash clone();

   /**
    * Retorna la cantidad de listas que posee la tabla de Hash
    */
    public int obtenerTamanoActualLista();

   /**
    * Retorna la cantidad de elementos de la lista
    */
    public int obtenerCantidadListaOcupado();
}