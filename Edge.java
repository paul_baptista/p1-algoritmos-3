/**
* Archivo: Edge.java
* Descripcion: Clase que almacena la informacion de una Arista (lado)
* Autor: Eduardo Blanco
* Fecha: Sep 2010
*/

public class Edge {

   private String src = null;
   private String dst = null;

   /* 
      Constructor de uso exclusivo de la clase edge.
   */
   private Edge() {
   }
   /**
   * Crea una arista entre los vertices src y dst.
   */
   public Edge(String src, String dst) {
      this.src=src;
      this.dst=dst;
   }

   /**
   * Retorna una nueva arista que es copia de this.
   */
   @Override
   protected Object clone() {
   Edge ed = new Edge();

   // se copian (clonan) todos los objetos internos, 
   // no solo asignar las referencias
   ed.src = new String(src);
   ed.dst = new String(dst);

   return ed;
   }

   /**
   * Indica si la arista de entrada es igual a this.
   */
   public boolean equals(Object o) {

   Edge d;

   if (!(o instanceof Edge))
      return false;

   d = (Edge) o;

      return d.src.equalsIgnoreCase(src)&&d.dst.equalsIgnoreCase(dst);
   //	Ver Node.equals()

      // throw new UnsupportedOperationException("Not supported yet.");
   }

   /**
   * Retorna el vertice src de la arista.
   */
   public String getSrc() {
      String d = new String(this.src);
      return d;
      // throw new UnsupportedOperationException("Not supported yet.");
   }

   /**
   * Retorna el vertice dst de la arista.
   */
   public String getDst() {
      String d = new String(this.dst);
      return d;
      // throw new UnsupportedOperationException("Not supported yet.");
   }


   /**
   * Verificar si el destino del arco es "n"
   */
   public boolean verificarDst(String n) {
     return n.equalsIgnoreCase(dst);
   }

   /**
   * Verificar si el origen del arco es "n"
   */
   public boolean verificarSrc(String n) {
     return n.equalsIgnoreCase(src);
   }

   
   /**
   * Retorna la representacion en String de la arista.
   */
   @Override
   public String toString() {
   return "("+src + ", " + dst+")";
   }

}

// End Edge.java
